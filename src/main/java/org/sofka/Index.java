package org.sofka;

import org.sofka.punto1.Numero;
import org.sofka.punto11.TestPuntoOnce;
import org.sofka.punto12.TestPuntoDoce;
import org.sofka.punto13.Hora;
import org.sofka.punto14.TestPuntodoCatorce;
import org.sofka.punto15.TestPuntoQuince;
import org.sofka.punto16.Persona;
import org.sofka.punto17.Electrodomestico;
import org.sofka.punto17.Lavadora;
import org.sofka.punto17.Television;
import org.sofka.punto18.Serie;
import org.sofka.punto18.VideoJuego;
import org.sofka.punto3.AreaCirculo;
import org.sofka.punto4.Iva;
import org.sofka.punto5.Test;
import org.sofka.punto6.ParImpar;
import org.sofka.punto7.MayorCero;
import org.sofka.punto8.TestPuntoOcho;
import org.sofka.punto9.TestPuntoNueve;
import org.sofka.punto10.TestPuntoDiez;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Index {
    private static final Logger logger = Logger.getLogger(String.valueOf(Index.class));
    public static void main(String[] args) {
        int opcion;
        do{
            opcion=Integer.parseInt(JOptionPane.showInputDialog("********** Menu de Ejercicios **********\n1." +
                    " Identificar el número mayor.\n2." +
                    "Identificar si un némero es mayor, menor o igual a otro.\n3" +
                    "Hallar el área de un círculo.\n4." +
                    "Calcular el precio de un producto con Iva incluido.\n5" +
                    "Indica cuáles son los números pares e impares del 1 al 100 (while).\n6." +
                    "Indica cuáles son los números pares e impares del 1 al 100 (for).\n7." +
                    "Confirma si un número es mayor a cero.\n8." +
                    "Verifica si un día de la semana es laboral o no.\n9" +
                    "Reemplaza la vocal (a) por (e).\n10." +
                    "Elimina espacios entre palabras.\n11." +
                    "Determinar longitud de una frase y cantidad de vocales en esta.\n12." +
                    "Diferencias entre dos palabras po frases.\n13." +
                    "Consulta fecha y hora actual.\n14." +
                    "Ingrese número para imprimir con saltos de 2 en dos hasta llegar a 1000\n15." +
                    "Mostrar menú Gestión cinematográfica.\n16." +
                    "probar la calse Persona\n17." +
                    "probar la clase Electrodomestico\n18." +
                    "salir"));
            switch (opcion){
                case 1:
                    Numero numero = new Numero(1, 4);
                    numero.findlargestnumber();
                    break;
                case 2:
                    int numberone= Integer.parseInt(JOptionPane.showInputDialog("Digita el Primer numero"));
                    int numbertwo=Integer.parseInt(JOptionPane.showInputDialog("Digita el  Segundo Numero "));
                    Numero numero1 = new Numero(numberone,numbertwo);
                    break;
                case 3:
                    AreaCirculo area = new AreaCirculo();
                    double radio=Double.parseDouble(JOptionPane.showInputDialog("digite el radio para calcular el area del circulo"));
                    JOptionPane.showMessageDialog(null,"EL area del circulo es :"+area.Area(radio));
                    break;
                case 4:
                    Iva iva = new Iva();
                    double precio= Double.parseDouble(JOptionPane.showInputDialog("Ingrese prescio del producto: "));
                    JOptionPane.showMessageDialog(null,"El precio final del producto con Iva incluido es de:  $"+iva.precioIva(precio));
                    break;
                case 5:
                    Test tes05 =new Test();
                    tes05.testPuntocinco();
                    break;
                case 6:
                    ParImpar n= new ParImpar();
                    n.ParImparFor();
                    break;
                case 7:
                    MayorCero mayor=new MayorCero();
                    mayor.mayorcero();
                    break;
                case 8:
                    TestPuntoOcho testPuntoOcho=new TestPuntoOcho();
                    testPuntoOcho.testpuntoOcho();
                    break;
                case 9:
                    TestPuntoNueve testPuntoNueve=new TestPuntoNueve();
                    testPuntoNueve.tespuntonueve();
                    break;
                case 10:
                    TestPuntoDiez testPuntoDiez=new TestPuntoDiez();
                    testPuntoDiez.tespuntodiez();
                    break;
                case 11:
                    TestPuntoOnce testPuntoOnce=new TestPuntoOnce();
                    testPuntoOnce.testpuntoOnce();
                    break;
                case 12:
                    TestPuntoDoce testPuntoDoce=new TestPuntoDoce();
                    testPuntoDoce.testpuntodoce();
                    break;
                case 13:
                    Hora hora = new Hora();
                    hora.fecha();
                    break;
                case 14:
                    TestPuntodoCatorce testPuntodoCatorce=new TestPuntodoCatorce();
                    testPuntodoCatorce.testPuntoCatorce();
                    break;
                case 15:
                    TestPuntoQuince testPuntoQuince=new TestPuntoQuince();
                    testPuntoQuince.testpuntoquince();
                    break;
                case 16:
                    final int SOBREPESO = 1;
                    final int PESO_IDEAL = 0;
                    final int PESO_POR_DEBAJO_IDEAL = -1;

                    InputStreamReader in = new InputStreamReader(System.in);
                    BufferedReader buffer = new BufferedReader(in);
                    String nombre = "";
                    char sexo = 0;
                    int edad = 0;
                    float peso = 0;
                    float altura = 0;

                    nombre=JOptionPane.showInputDialog("Ingrese el nombre");
                    edad=Integer.parseInt(JOptionPane.showInputDialog("Ingrese la edad"));
                    peso=Float.parseFloat(JOptionPane.showInputDialog("Ingrese el peso"));
                    altura=Float.parseFloat(JOptionPane.showInputDialog("Ingrese la altura"));



                    Persona p1 = new Persona(nombre,edad,sexo,peso,altura);
                    Persona p2 = new Persona(nombre,edad,sexo);
                    Persona p3 = new Persona();

                    p3.setNombre(nombre);
                    p3.setEdad(edad);
                    p3.setAltura(altura);
                    p3.setPeso(peso);
                    p3.setSexo(sexo);

                    Persona[] personas ={p1,p2,p3};
                    for (Persona persona : personas) {
                        //System.out.println(persona.toString());
                        switch (persona.calularIMC()) {
                            case SOBREPESO:
                                break;
                            case PESO_IDEAL:
                                JOptionPane.showMessageDialog(null,"tiene un peso ideal");
                                //System.out.println("tiene un peso ideal");
                                break;
                            case PESO_POR_DEBAJO_IDEAL:
                                JOptionPane.showMessageDialog(null,"tiene un peso por debajo del ideal");
                                //System.out.println("tiene un peso por debajo del ideal");
                                break;
                        }
                        if (persona.serMayorDeEdad()) {
                            JOptionPane.showMessageDialog(null,"es mayor de edad");
                            //System.out.println("es mayor de edad");
                        } else {
                            //System.out.println("no es mayor de edad");
                            JOptionPane.showMessageDialog(null,"no es mayor de edad");
                        }
                    }
                    break;
                case 17:
                    Electrodomestico[] electrodomesticos = new Electrodomestico[10];

                    electrodomesticos[0] = new Lavadora();
                    electrodomesticos[1] = new Lavadora(30.0, "NEGRO", 'D', 4.4, 3.0);
                    electrodomesticos[2] = new Lavadora(25.0, 2.3);
                    electrodomesticos[3] = new Lavadora(10.0, 3.1);
                    electrodomesticos[4] = new Television();
                    electrodomesticos[5] = new Television(50.2, 3.0);
                    electrodomesticos[6] = new Television(18.3, 4.2);
                    electrodomesticos[7] = new Television(31.0, "AZUL", 'A', 5.2, 42.0, true);
                    electrodomesticos[8] = new Lavadora();
                    electrodomesticos[9] = new Television();

                    for (Electrodomestico electrodomestico : electrodomesticos){
                        logger.log(Level.INFO, " {0}", electrodomestico);
                    }

                    Double sumaElectrodomesticosGenerales = 0.0;
                    Double sumaLavadoras = 0.0;
                    Double sumaTelevisores = 0.0;

                    for (Electrodomestico electrodomestico : electrodomesticos){
                        if (electrodomestico instanceof Lavadora){
                            sumaLavadoras += electrodomestico.precioFinal();
                        }else if (electrodomestico instanceof Television){
                            sumaTelevisores += electrodomestico.precioFinal();
                        }else {
                            sumaElectrodomesticosGenerales += electrodomestico.precioFinal();
                        }
                    }
                    Double sumaTotal = sumaElectrodomesticosGenerales + sumaLavadoras + sumaTelevisores;
                    String message = "Precio total: " + sumaTotal + " (Generales: " + sumaElectrodomesticosGenerales +
                            " Lavadoras: " + sumaLavadoras + " Televisiones: " + sumaTelevisores + ")";
                    logger.log(Level.INFO, " {0}", message);

                    break;



            }

        }while(opcion!=18);

    }
}
