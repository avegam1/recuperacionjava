package org.sofka.punto14;

import javax.swing.*;

public class SaltosDeDos {
    private int numero;

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void ImprimirNumeroConsaltosdeDos(int numero){

        while(numero  < 1000){
            JOptionPane.showMessageDialog(null,"Numero: "+numero);
            numero+=2;
        }
    }
}
